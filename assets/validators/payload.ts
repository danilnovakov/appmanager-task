import Joi from "joi";
import { ImageCheckboxesState, ImagePayload } from "../types";
import {
  ALLOWED_IMAGE_MIME_TYPES,
  MAX_IMAGE_HEIGHT,
  MAX_IMAGE_WIDTH,
} from "../constants";

export const validateImagePayload = (
  payload: ImagePayload,
  checkboxesState: ImageCheckboxesState
) => {
  return Joi.object({
    imageWidth: checkboxesState.isScaleChecked
      ? Joi.number().min(1).max(MAX_IMAGE_WIDTH)
      : Joi.optional(),
    imageHeight: checkboxesState.isScaleChecked
      ? Joi.number().min(1).max(MAX_IMAGE_HEIGHT)
      : Joi.optional(),
    file: Joi.any().custom((v: File, h) => {
      return ALLOWED_IMAGE_MIME_TYPES.includes(v.type)
        ? v
        : h.error("invalid-image-type");
    }),
  })
    .messages({
      "invalid-image-type": "Only phg images are supported",
    })
    .validate(payload);
};
