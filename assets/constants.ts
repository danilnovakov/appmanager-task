export const ALLOWED_IMAGE_MIME_TYPES = ["image/png"];
export const UPLOAD_IMAGE_URL = "image/upload";
export const MAX_IMAGE_WIDTH = 7680;
export const MAX_IMAGE_HEIGHT = 4320;
