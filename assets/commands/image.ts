import { ImagePayload } from "../types";
import { UPLOAD_IMAGE_URL } from "../constants";
import { createImageFormData } from "../ creators/image";

export const uploadImageCommand = async (payload: ImagePayload) => {
  return fetch(UPLOAD_IMAGE_URL, {
    method: "POST",
    body: createImageFormData(payload),
  })
    .then((res) => {
      if (res.status !== 200) throw new Error("Invalid request");

      return res;
    })
    .then((response) => response.json());
};
