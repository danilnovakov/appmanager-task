import React, { ChangeEvent, useRef } from "react";
import { Checkbox } from "./form/Checkbox";
import { validateImagePayload } from "../validators/payload";
import { Empty } from "./generic/Empty";
import { uploadImageCommand } from "../commands/image";
import { ImageCheckboxesState } from "../types";
import { createImagePayload } from "../ creators/image";

export const FileUploader: React.FC = () => {
  const [image, setImage] = React.useState<File | null>(null);
  const [imageWidth, setImageWidth] = React.useState(0);
  const [imageHeight, setImageHeight] = React.useState(0);
  const [formMessage, setFormMessage] = React.useState<string | null>(null);
  const scaleSelectorRef = useRef<HTMLInputElement>(null);

  const imageHandler = (e: ChangeEvent<HTMLInputElement>) => {
    if (null !== e.target.files) {
      setImage(e.target.files[0]);
    }
  };
  const imageWidthHandler = (e: ChangeEvent<HTMLInputElement>) =>
    setImageWidth(parseInt(e.target.value));
  const imageHeightHandler = (e: ChangeEvent<HTMLInputElement>) =>
    setImageHeight(parseInt(e.target.value));

  const handleUpload = async () => {
    const checkboxesState: ImageCheckboxesState = {
      isScaleChecked: Boolean(scaleSelectorRef.current?.checked),
    };

    const payload = createImagePayload(
      image!,
      imageWidth,
      imageHeight,
      checkboxesState
    );
    const validationResult = validateImagePayload(payload, checkboxesState);

    if (validationResult.error) {
      for (const err of validationResult.error.details.values()) {
        setFormMessage(err.message);
        break;
      }

      return;
    }

    try {
      await uploadImageCommand(validationResult.value);

      setFormMessage("Image successfully uploaded");
    } catch {
      setFormMessage("Failed to upload image");
    }
  };

  return (
    <form method="post" encType="multipart/form-data">
      <React.Fragment>
        <span>Image: </span>
        <input
          onChange={imageHandler}
          type="file"
          accept={"image/png"}
          id="image"
          name="image"
        ></input>
      </React.Fragment>
      <br></br>
      <React.Fragment>
        <Checkbox
          ref={scaleSelectorRef}
          isVisible={true}
          title="Resize image"
          showOnChecked={
            <React.Fragment>
              <span>Width: </span>
              <input
                onChange={imageWidthHandler}
                type="number"
                id="width"
                name="image-width"
              ></input>
              <span>Height: </span>
              <input
                onChange={imageHeightHandler}
                type="number"
                id="height"
                name="image-height"
              ></input>
              <br></br>
            </React.Fragment>
          }
        ></Checkbox>
      </React.Fragment>
      <br></br>
      {null !== image ? (
        <button type="button" onClick={handleUpload}>
          Upload
        </button>
      ) : (
        <Empty></Empty>
      )}
      <br></br>
      {null !== formMessage ? <span>{formMessage}</span> : <Empty></Empty>}
    </form>
  );
};
