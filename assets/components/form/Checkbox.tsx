import React from "react";
import { Empty } from "../generic/Empty";

interface CheckboxProps {
  isVisible: boolean;
  title: string;
  showOnChecked: JSX.Element;
}

export const Checkbox = React.forwardRef<HTMLInputElement, CheckboxProps>(
  (props, ref) => {
    const [isChecked, setIsChecked] = React.useState(false);

    const checkHandler = () => {
      setIsChecked(!isChecked);
    };

    return (
      <React.Fragment>
        {props.isVisible ? (
          <React.Fragment>
            <br></br>
            <span>{props.title}</span>
            <input
              ref={ref}
              type="checkbox"
              name="address"
              checked={isChecked}
              onChange={checkHandler}
            />
            {isChecked ? (
              props.showOnChecked
            ) : (
              <React.Fragment></React.Fragment>
            )}
          </React.Fragment>
        ) : (
          <Empty></Empty>
        )}
      </React.Fragment>
    );
  }
);
