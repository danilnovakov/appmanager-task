export interface ImagePayload {
  imageWidth?: number;
  imageHeight?: number;
  file: File;
}

export interface ImageCheckboxesState {
  isScaleChecked: boolean;
}
