import ReactDOM from "react-dom";
import { FileUploader } from "./components/FileUploader";

document.addEventListener("DOMContentLoaded", function () {
  ReactDOM.render(
    <>
      <FileUploader></FileUploader>
    </>,
    document.getElementById("root")
  );
});
