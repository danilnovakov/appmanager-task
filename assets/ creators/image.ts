import { ImageCheckboxesState, ImagePayload } from "../types";

export const createImageFormData = (payload: ImagePayload): FormData => {
  const formData = new FormData();

  for (const [k, v] of Object.entries(payload)) {
    formData.append(`image[${k}]`, v);
  }

  return formData;
};

export const createImagePayload = (
  image: File,
  imageWidth: number,
  imageHeight: number,
  checkboxesState: ImageCheckboxesState
): ImagePayload => {
  const payload: ImagePayload = {
    file: image!,
  };

  if (checkboxesState.isScaleChecked) {
    payload.imageWidth = imageWidth;
    payload.imageHeight = imageHeight;
  }

  return payload;
};
