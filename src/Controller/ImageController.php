<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use App\Repository\ImageRepository;
use App\Service\FilenameProviderInterface;
use App\Service\ImageManipulatorInterface;
use App\Service\ImageUploaderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
    #[Route('/image/upload', name: 'app_image_upload', methods: ['POST'])]
    public function upload(
        Request $request,
        ImageRepository $imageRepository,
        ImageUploaderInterface $fileUploader,
        ImageManipulatorInterface $imageManipulator,
        FilenameProviderInterface $filenameProvider
    ): JsonResponse
    {
        $form = $this->createForm(ImageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = new Image();

            $originalFile = $form->get(ImageType::FILE_FIELD_NAME)->getData();
            $resizeWidth = $form->get(ImageType::IMAGE_WIDTH_FIELD_NAME)->getData();
            $resizeHeight = $form->get(ImageType::IMAGE_HEIGHT_FIELD_NAME)->getData();

            $originalFilename = $filenameProvider->getFilename($originalFile);
            $resizedFilename = $filenameProvider->getFilename($originalFile);

            $file = $fileUploader->upload($originalFile, $originalFilename);

            if (null !== $resizeWidth && null !== $resizeHeight) {
                $imageManipulator->resize($file, $resizeWidth, $resizeHeight, $resizedFilename);

                $image->setModifiedName($resizedFilename);
            }

            $imageRepository->save(
                $image
                    ->setOriginalName($originalFilename)
                    ->setTimeCreated(new \DateTime()),
                true
            );

            return new JsonResponse(['message' => 'File successfully uploaded']);
        }

        return new JsonResponse(['message' => 'Unable to upload file'], 500);
    }
}
