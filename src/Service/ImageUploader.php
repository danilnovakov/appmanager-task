<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

readonly class ImageUploader implements ImageUploaderInterface
{
    public function __construct(
        private ParameterBagInterface $params
    ){}

    /**
     * @throws FileException
     */
    public function upload(UploadedFile $file, string $targetFilename): File
    {
        return $file->move($this->params->get('uploads_directory'), $targetFilename);
    }
}