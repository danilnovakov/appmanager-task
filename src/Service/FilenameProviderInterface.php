<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FilenameProviderInterface
{
    public function getFilename(UploadedFile $file);
}