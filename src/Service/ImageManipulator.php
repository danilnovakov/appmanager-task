<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;

readonly class ImageManipulator implements ImageManipulatorInterface
{
    private Imagine $imagine;

    public function __construct(private ParameterBagInterface $parameterBag)
    {
        $this->imagine = new Imagine();
    }

    public function resize(File $file, int $width, int $height, string $targetFilename): void
    {
        $localFile = $this->imagine->open($file->getPathname());

        $localFile->resize(new Box($width, $height))
            ->save(
                $this->parameterBag->get('uploads_directory')
                . '/'
                . $targetFilename
            );
    }
}