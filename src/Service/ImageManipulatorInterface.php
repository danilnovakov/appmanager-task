<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

interface ImageManipulatorInterface
{
    public function resize(File $file, int $width, int $height, string $targetFilename): void;
}