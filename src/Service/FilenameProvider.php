<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

readonly class FilenameProvider implements FilenameProviderInterface
{
    private const MAX_TRIES = 4;

    public function __construct(private ParameterBagInterface $parameterBag) {}

    /**
     * @throws FileException
     */
    public function getFilename(UploadedFile $file): string {
        $uploadsDir = $this->parameterBag->get('uploads_directory');

        for ($i = 0; $i < self::MAX_TRIES; $i++) {
            $filename = uniqid() . '.' . $file->guessExtension();

            if (!file_exists($uploadsDir . '/' . $filename)) {
                return $filename;
            }
        }

        throw new FileException(
            'Unable to generate unique filename(file exists), tried ' . self::MAX_TRIES . ' times'
        );
    }
}