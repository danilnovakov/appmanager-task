<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

class ImageType extends AbstractType
{
    public const MAX_IMAGE_WIDTH = 7680;
    public const MAX_IMAGE_HEIGHT = 4320;
    public const FILE_FIELD_NAME = 'file';
    public const IMAGE_WIDTH_FIELD_NAME = 'imageWidth';
    public const IMAGE_HEIGHT_FIELD_NAME = 'imageHeight';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::FILE_FIELD_NAME, FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Constraints\File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Invalid image type',
                    ])
                ],
            ])
            ->add(self::IMAGE_WIDTH_FIELD_NAME, NumberType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Constraints\GreaterThan(0),
                    new Constraints\LessThan(self::MAX_IMAGE_WIDTH + 1)
                ]
            ])
            ->add(self::IMAGE_HEIGHT_FIELD_NAME, NumberType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Constraints\GreaterThan(0),
                    new Constraints\LessThan(self::MAX_IMAGE_HEIGHT + 1)
                ]
            ])
        ;
    }
}
