#!/bin/bash

docker-compose up -d
docker exec -it appmanager-task-php-1 composer install
docker exec -it appmanager-task-php-1 yarn install
docker exec -it appmanager-task-php-1 yarn build
docker exec -it appmanager-task-php-1 php bin/console --no-interaction doctrine:migrations:migrate
