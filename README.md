# Symfony + NGINX + MariaDB + Docker 

## Jak uruchomić aplikację
```shell
./start.sh
```
Apka będzie dostępna pod adresem http://localhost:8010

### Co się udało zrobić:
* Postawić Symfony + Nginx + MariaDB w Docker'ze, frontend React + TypeScript
* Stworzyć migrację
* Stwórzyć warstwę wizualną aplikacji
* Upload pliku
* Resize pliku do wybranej rozdzielczości
* Historia plików w bazie danych

### Co się nie udało zrobić przez brak czasu:
* Konwertowanie pliku do wybranego
* Konwersja plikow
* Zbieranie statystyk w Redis'ie
* Stylowanie widoku
* Grid z historią plików w widoku
* Testy PHPUnit
* Testy Funkcyjne/Integracyjne
* Testy Cypress(e2e)